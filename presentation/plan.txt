En l'occurence je dois présenter Kubernetes à une dizaine de personnes.

L'idée est de faire un peu de théorie mais aussi de manipuler et
d'initier à des choses concrètes.

Mon idée de plan est :

# 0. cet atelier

## description
C'est :
- mieux comprendre k8s
- s'initier à la manipulation des objets de base

Ce n'est pas :
- une explication des spécifités Radio France
- une formation complète sur k8s

## objectif

À la fin de la session vous saurez:
- ce qu'est un Pod, un Deployment et un Service
- utiliser kubectl
- pour créer un Pod, un Deployment et un Service
- pour regarder l'état de ces objets
- ce que c'est qu'un rolling-update
- ce que c'est que la Readiness



# 1. Kubernetes c'est
## un système d'exploitation pour les Containers

Il gère :
- l'allocation des ressources aux containers (CPU, RAM, stockage)
- le cycle de vie des containers
- la communication entre les containers

## une base de données partagée accessible via une API

Le coeur de Kubernetes est ce qu'on appelle son API.

En réalité cette API est juste une interface REST devant une base de
données objet.

> The Kubernetes API is a resource-based (RESTful) programmatic interface
> provided via HTTP. It supports retrieving, creating, updating, and
> deleting primary resources via the standard HTTP verbs (POST, PUT,
> PATCH, DELETE, GET), includes additional subresources for many objects
> that allow fine grained authorization (such as binding a pod to a node),
> and can accept and serve those resources in different representations for
> convenience or efficiency. It also supports efficient change notifications
> on resources via "watches" and consistent lists to allow other components
> to effectively cache and synchronize the state of resources.

Dans cette API on agit sur des objets qu'on appelle "resources".

Les ressources les plus utilisées sont :
- Node
- Namespace
- Pod
- Deployment
- StatefulSet
- Node
- Service
- Ingress
- ConfigMap
- Secret
- PersistentVolume

Il y en a plein d'autres, et on peut même en créer des nouvelles.

Chaque objet a un schéma bien précis.



Vous noterez que le mot "Container" n'apparait pas dans la liste.

## un ensemble de Contrôleurs

Quand on interagit avec k8s on interagit avec l'API. Mais l'API n'est
qu'une base de donnée.

Pour les véritables actions, k8s est en fait un assemblage de
contrôleurs qui sont des processus indépendant les uns des autres qui
exécutent une boucle de réconciliation.

[!schéma d'une boucle de réconciliation]

## les contrôleurs principaux

Parmi les contrôleurs principaux on a
- kube-scheduler
- kube-controller-manager (replication controller, endpoints controller, namespace controller, serviceaccounts controller...)
- Cloud controller manager 


# TP: skillsscloudboost et kubectl
- se connecter à la console
- créer le cluster
- kubectl get namespaces
- kubectl --namespace kube-system get pods
- kubectl --namespace kube

## Mon premier Namespace et mon premier Pod

# C'est quoi un Pod ?

Le Pod est l'unité de base manipulée par kubernetes.


# 4. Présentation de la notion de Pod (unité de base constituée d'un
ensemble atomique de containers), de Node (substrat "physique" sur
lequel on peut exécuter des Pods) et de Workload (regroupement de Pods
similaires/identiques pour effectuer une tâche donnée)

# 5. Premier TP, introduction aux outils
- utilisation de la CLI kubectl
- configuration du fichier kubeconfig et authentification au cluster

# 6. Deuxième TP, manipuler un pod
- création d'un pod avec un seul container dedans
- différentes phases de création d'un pod
- accès à l'état du pod (`kubectl get -o yaml` et `kubectl describe`)
- accès aux logs générés par le pod
- destruction du pod

# 7. Retour théorique sur la création d'un Pod. Les différents controller
impliqués et un exemple d'interaction entre eux par l'intermédiaire
du stockage du kubeapi.

# 8. Troisième TP, les Deployments
- création d'un Deployment (le Workload le plus simple) avec 1 seul
  réplica (un seul pod)
- modification du Deployment en utilisant `kubectl patch` pour passer
  le nombre de réplicas à 3
- création d'un HorizontalPodAutoscaler pour voir ce qui se passe
  (il va modifier le nombre de réplicas)

# 9. Notion de label et importance du concept pour toutes les ressources
kubernetes.

# 10. La communication au sein du cluster. IPs affectées aux pods. Notion
de Service (load-balancer virtuel permettant d'accéder à plusieurs pods)

# 11. Troisième TP, Services
- création d'un Service pour accéder aux pods du Deployment
- création d'un Pod qui appelle le Service
- visualisation de la répartition de charge

# 12. Notion de Liveness et Readiness

# 13. Quatrième TP, rolling-update
- modification du Deployment
- observer les étapes (création de nouveau pod, les nouveaux pods
  deviennent Ready, les anciens sont détruits)

# 13. La communication depuis l'extérieur du Cluster, les Ingress

# 14. Conclusion
Too many YAML, comment on gère à Radio France.

# Création d'un secret Docker pour utiliser les images du hub
kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v2/ --docker-username=rfatelierk8s --docker-password=85b27301-e6d8-4bd4-a6fd-9c6a225cdc91


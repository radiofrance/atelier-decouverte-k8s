# Création du namespace

On recréé le Namespace qu'on avait supprimé.

    kubectl apply -f unjolinamespace.yaml

# Création d'un Deployment

On peut créer un Deployment qui est une façon de gérer des Pods.

    kubectl apply -f simple-deployment.yaml

Si on regarde bien on voit les pods liés au Deployment être créés
par le contrôleur de Deployment.

    kubectl --namespace unjolinamespace get pod

On peut regarder à quoi ressemble la ressource Deployment.

     kubectl --namespace unjolinamespace get deployment -o yaml simple

Regarder la ressource correspondant à un des Pods nouvellement créés
et observer les champs :
- .metadata.ownerReferences
- .spec.nodeName
- .status.podIP
- .status.StartTime

# Modification du Deployment

On applique le même déploiement mais légèrement modifié (la commande
exécutée est changée).

    kubectl apply -f simple-deployment.modified.yaml

On peut voir le mécanisme de de RollingUpdate s'opérer (création de
nouveaux pods avant la suppression des anciens).

    watch kubectl --namespace unjolinamespace get pod

On peut ensuite regarder les logs d'un pod et voir la différence.

# Création d'un namespace

On crée le namespace

    kubectl apply -f unjolinamespace.yaml

On peut regarder la ressource complète après complétion par l'API et
les Contrôleurs.

    kubectl get namespace unjolinamespace -o yaml

# Création d'un Pod

On créé un pod tout simple qui écrit la date sur sa sortie standard
environ une fois par seconde.

    kubectl apply -f simple-pod.yaml

On regarde la ressource du pod complétée. Faire attention au champ
`.spec.nodeName` et aux champs dans `.status`.

    kubectl get pod --namespace unjolinamespace -o yaml simple

On peut aussi voir les logs du Pod.

    kubectl --namespace unjolinamespace logs simple

Note: pour éviter de répéter le `--namespace` systématiquement, changez votre
contexte avec `kubectl config set-context --current --namespace unjolinamespace`

# Suppression du namespace

    kubectl delete --wait=false namespace unjolinamespace

On peut faire ces commandes plusieurs fois pour observer l'évolution
pendant que les différents Contrôleurs prennent en compte la demande
de suppression.

    kubectl get pod --namespace unjolinamespace
    kubectl get secrets --namespace unjolinamespace
    kubectl get namespace

    watch '
      kubectl get pod --namespace unjolinamespace;
      kubectl get secrets --namespace unjolinamespace;
      kubectl get namespace;'

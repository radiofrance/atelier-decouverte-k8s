# Prérequis

Dans ce TP nous allons utiliser le lab Google Cloudskillsboost, mettre en place
les éléments nécessaires à la réalisation des TPs suivant et commencer à
utiliser `kubectl`

## connexion à cloudskillsboost

## récupération du TP

      git clone https://gitlab.com/radiofrance/atelier-decouverte-k8s
      cd atelier-decouverte-k8s/tp/00

## Création du cluster

      gcloud auth list
      gcloud config list projects
      gcloud config list project
      gcloud config set compute/zone us-central1-b
      gcloud container clusters create io

La création du cluster prend plusieurs minutes.

## premiers pas

On jette un coup d'oeil avec `kubectl` pour voir ce qu'il y a dans le
nouveau cluster.

    kubectl get nodes
    kubectl get namespaces
    kubectl --namespace kube-system get pods
    kubectl get all
    kubectl get all --all-namespaces

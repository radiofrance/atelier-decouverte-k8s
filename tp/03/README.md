# Création d'un Deployment "serveur"

Notre Deployment "serveur" se contente de copier dans ses logs ce qu'il
reçoit. Avant de le déployer on observe le YAML et en particulier les
champs suivant :
- .spec.selector
- .spec.template.metadata.labels
- .spec.template.spec.containers[].ports
- .spec.template.spec.containers[].readinessProbe

On créé le Deployment.

    kubectl apply -f server-deployment.yaml

On voit les pods liés au Deployment être créés par le contrôleur
de Deployment.

    kubectl --namespace unjolinamespace get pod

On peut regarder à quoi ressemble la ressource Deployment.

     kubectl --namespace unjolinamespace get deployment -o yaml simple

Regarder la ressource correspondant à un des Pods nouvellement créés
et observer les champs :
- .metadata.ownerReferences
- .spec.nodeName
- .status.podIP
- .status.StartTime
- .metadata.labels

Regarder les logs d'un des Pods server.

# Création d'un Deployment "client"

Création :

    kubectl apply -f client-deployment.yaml

On peut observer comme d'habitude les pods et leurs logs. Et voir les
erreurs. (Les lignes vides sur le server sont liées à la Liveness)

# Création d'un Service "allserver"

On peut regarder dans le fichier server-service.yaml le champ
.spec.selector et comparer avec le champ .spec.selector du Deployment.

On peut aussi regarder les champs .metadata.labels des Pods.

    kubectl apply -f server-service.yaml

À ce moment si on observe les logs des différents Pods on voit que la
communication est effective.

On peut regarder comment fonctionne le controller de Service

    kubectl get service server -o wide
    kubectl get endpoints server -o wide

# Modification du Deployment server

    kubectl apply -f server-deployment.modified.yaml

On peut voir le mécanisme de de RollingUpdate s'opérer (création
de nouveaux pods avant la suppression des anciens). Pendant la durée
d'initialisation les Pods ne sont pas « Ready ».

    watch kubectl --namespace unjolinamespace get pod

# Ajout d'un deuxième Deployment "serverbis"

    kubectl apply -f serverbis-deployment.yaml

Comparer:
- .spec.selector du Service
- .spec.selector.matchLabels du Deployment "server"
- .spec.selector.matchLabels du Deployment "serverbis"
- .spec.template.metadata.labels du Deployment "server"
- .spec.template.metadata.labels du Deployment "serverbis"

Regarder les logs des pods clients.

# atelier-decouverte-k8s

Ce dépot Git contient le contenu de l'atelier « Introduction à Kubernetes »
qu'on fait une fois de temps au sein de la Direction du Numérique.

Il contient le support de présentation et ses sources dans le répertoire
`presentation` ainsi que les éléments pour les Travaux Pratiques dans
les répertoires `tp/00` à `tp/03`.

